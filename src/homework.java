


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

public class homework {
	public static void main(String[] args){
		ArrayList<Double> myArray = new ArrayList<Double>();
		ArrayList<String> array1 = new ArrayList<String>();
		String filename = "homework.txt";
		FileReader fileReader = null;
		FileWriter fileWriter = null;
		
		try {
		    // read from user
			String name = " ";
			fileReader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(fileReader);
			String line;
			for (line = buffer.readLine(); line != null;line = buffer.readLine()){
				String s4[] = line.split(", ");
				
				for(int n = 1 ; n<s4.length ; n++){
					double b = Double.parseDouble(s4[n]);
					myArray.add(b);
					
					
				}
				double ave = average(myArray);
				array1.add(s4[0]+" "+ave);
				
			}
			fileWriter = new FileWriter("average.txt");  //ถ้าต้องการเพิ่มข้อความต่อท้ายโดยไม่ลบอันเก่าให้เติม true หลัง from-user.txt เช่น "from-user.txt", true
		    PrintWriter out = new PrintWriter(fileWriter);
		    for(int x = 0 ; x<array1.size() ; x++){
		    	out.println(name + array1.get(x));
		    	out.flush();  //gon't forget
				
		  
		  
			}
		}
		catch (IOException e){
            System.err.println("Error reading from user");
			}
		
		finally {
			System.out.println("IN FINALLY");
		    try {
		       if (fileWriter != null)
		    	   fileWriter.close();
		    } 
		    
		    catch (IOException e) {
		       System.err.println("Error closing files");
		    } 
		}
		}
	
	public static double average(ArrayList<Double> b){
		double sum = 0;
		for(int a=0 ; a<b.size();a++){
			sum = sum+b.get(a);
			
		}
		return sum/b.size();
		
	}
}
	



